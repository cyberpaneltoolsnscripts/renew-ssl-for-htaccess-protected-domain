#!/usr/bin/env bash
# Usage: Renews domain/subdomain protected by htaccess.
# Create a backup of the always correct file to put back after renewal as "htaccess_backup"
# Example:
# cp -a /home/somedomain.tld/public_html/.htaccess /home/somedomain.tld/public_html/.htaccess_original

## Configure:
## this file is cleared during renewal
htaccess_live='/home/somedomain.tld/public_html/.htaccess'

## this file is the real file and should be the only one ever edited and should be copied over the live one if it is updated
htaccess_backup='/home/somedomain.tld/public_html/.htaccess_original'

# Domain/s to renew
domain_renew='somedomain.tld'

# Clears the live file
> "${htaccess_live}"

# Renews
cyberpanel issueSSL --domainName ${domain_renew} ; 

# Puts the original htaccess back afterwards
rsync -a "${htaccess_backup}" "${htaccess_live}" ;
