# Usage: 
Renews domain/subdomain protected by htaccess.

# How to use:
Create a backup of the desired htaccess file to put back after renewal as "htaccess_backup" option

Example:    
`cp -a /home/somedomain.tld/public_html/.htaccess /home/somedomain.tld/public_html/.htaccess_original`

Then edit the htaccess_live with the htaccess full file path:   
`htaccess_live='/home/somedomain.tld/public_html/.htaccess'`

Then edit the htaccess_backup with the htaccess full file path for the source file to restore after renewal:   
`htaccess_backup='/home/somedomain.tld/public_html/.htaccess'`

Next enter the Domain/s to renew:    
domain_renew='somedomain.tld'

Next save the file and then `chmod +x renew_ssl.sh` the file so it is executable.

Finally set it up as cron as desired. Recommend every 30 days so it has 2-3 attempts before expiring at the 90 day mark.   
`5 4 1 * * /root/renew_ssl.sh >/dev/null 2>&1`

Recommend using https://crontab.guru if you would like to visualize the time when picking it for an off peak time.

